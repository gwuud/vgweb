GITHUB STATUS APPLICATION
-------------
-------------

Development Environment:
-----------------------

- OpenSUSE Leap 15.1 (4.12.14-lp151.28.52-default)
- VirtualBox 6.0.18_SUSE r136238
- ubuntu/bionic64 (virtualbox, 20200618.0.0)
- Vagrant 2.2.9
- Ansible 2.9.6
- Python 3.6.10



Requirements:
------------

- Oracle VirtualBox*
- HashiCorp Vagrant
- Ansible 2.9.x
- Python3.x.x

*Because it is generally more accessible, this environment has been developed & tested in VirtualBox, but 
you are welcome to run this with the provider of your choice, though you should consider it untested and 
that there is the risk of error.



To run the environment:
----------------------
```
unzip vgweb.zip && cd vgweb
vagrant up --provider=virtualbox*
```

*For sustained use, you can also add the VAGRANT_DEFAULT_PROVIDER environment variable to your ~/.profile.

Overview:
--------
- Application makes an API call to Github and returns it's current operational status
- **Specification:**
  - Ubuntu/Bionic64 Vagrant Boxes (x3)
  - Vagrant user (by default), as well as members of *admin* group can sudo without requiring a password
  - Nginx Load Balancer (**AL01**)
  - Nginx Application Server Proxying to uWSGI Application (x2 - **AL02**, **AL03**)
  - Automated application and load-balancer tests, including asynchronous siege testing
  - Idempotent
  - Re-usable / modular code


Configuration Diagram:
---------------------
![Flow](flow.jpg)
**The image illustrates the flow of information between Ansible and Vagrant. By using roles and Jinja2 rendering, the majority of the structure's foundational 
code is reusable.**

Vagrantfile:
-----------

Beyond provisioning virtual machines, the Vagrantfile is also the point at which:
  - the infrastructure domain and upstream proxy names are declared and passed into Ansible as 'extra_vars'
  - resources are sorted (based on hostname) into Ansible groups 

*Any modification to the domain name or upstream server names should be made here.*



Ansible Stack Playbook (stack.yml):
----------------------------------

This is the backbone of the configuration. Where modularity has been considered in the development of this 
project, roles are being used to 'generally' configure resources, with this file acting as the "landing pad" 
for:
  - variables passed-in as extra_vars from Vagrant
  - declaring variables to pass-in to roles
  - anscilliary / light tasks

*Modification of the configuration (tweaks) should be applied here. Ie, load-balancing method:* 
```
  loadbalancer:
    method: ip_hash
```



Ansible Roles:
-------------

These have been designed for re-use and modularity. For details regarding the use of the accompanying Ansible 
roles, please refer to the role README.md.
It's also worth noting, that as this is a time-limited project, the number of variables currently available in 
any given role is limited to the requirements of the stack infrastructure.



Note on Role Variables:
----------------------

Variables are being declared in vars/main.yml under a dict named after the role. Ie, in Uwsgi_Application/vars/main.yml: 
```
  Uwsgi_Application:
    domain: "{{ domain }}"
```
This is purely a personal practise, where my experience in a large Ansible environment with numerous groups_vars, 
hosts_vars, vars vars vars is that it's easier to track where a mis-placed variable is being declared if it is 
prefixed with the role name or the group_var name it belongs to. In the case of the above:

Play:
```
domain: example.com
```
Role:
```
Uwsgi_Application.domain
```

It also forces vars/main.yml to become the point-of-truth for role variables, again, this can sometimes make tracking
a bit easier :)



YAML Variables:
--------------
YAML string variables are used throughout the code to ensure that certain string attributes remain linked. They have also been 
useful when linking attributes between plays (lb_config):
```
- name: LOAD BALANCER
  hosts: loadbalancers
  vars:
    config_file: &lb_config /etc/nginx/conf.d/load.conf

- name: CONFIGURATION TESTING
  hosts: all
  vars:
    loadbalancer: 
      config: *lb_config
```


Approximate Time Spent:
----------------------

50hrs (predominantly in role development), including research time



Strategy:
--------

My strategy, in the first instance, was to stand up all machines via the Vagrantfile and then configure the Nginx infrastructure 
manually. In this process, along with supplemented research, I began creating Nginx LoadBalancer and WebServer roles. At this 
point, the content being served was static, and I began testing how the load balancer functioned. This made for the basis of the 
configuration testing role. 
I continued the strategy throughout the project - branching, researching, running through a process manually, and recording it in a role.
Once the role was executing as intended, it was committed and merged to the master branch. This is the predominant order of execution that tried to I follow 
(not withstanding troubleshooting):
1. Initial Vagrantfile & VM configuration
2. Nginx application servers & role creation
3. Initial Stack playbook
4. Nginx load balancer & role creation
5. Diagram drawing / planning
6. Sudo tests & role creation
7. Configuration testing & role
8. Flask application & uWSGI framework testing, creation of role. Re-Work of Nginx application role
9. Code review
10. Annexation of Nginx application role tasks into uWSGI application role
11. Code review



Hindsight (Containerisation of Application):
---------

Although I feel I have been methodical in my approach to building out the IaC and initial backend infrastructure, I began the
task without being sure about what the endpoint would be - static site, dynamic site, etc. Once I had settled on a Flask 
application - because I know Python and I don't know PHP or JS, I then had to configure the application & servers for the uWSGI framework. Even though 
Nginx would then proxy to uWSGI, the co-existence of roles created idempotence issues. Therefore, and once the Nginx role had been 
effectively reduced down to installing the package and restarting the service, it was fully annexed into the uWSGI role. Time was
spent reverse-engineering and sanity-checking this, which, if I had a clearer outcome in mind, could have been time spent
containerising the application.



Troubleshooting:
---------------

- **Issue:** Nginx Serving Default Block
- **Resolution:** Remove symbolic link to sites-enabled 
```
sudo unlink /etc/nginx/sites-enabled/default
```

- **Issue:** Ansible "VARIABLE IS NOT DEFINED"
- **Resolution:** Use Ansible debug & setup modules to trace variables and Ansible facts
```
  - debug:
      var: var
```
```
  - setup:
```

- **Issue:** uWSGI fail to create socket (permission denied)
- **Resolution:** After examination of uWSGI & Nginx logs and supplemental research, adjusted POSIX permissions & ownership
```
sudo journalctl -u {app}
```
```
sudo tail -f /var/log/nginx/error.log
```
```
sudo tail -f /var/log/nginx/access.log
```
```
sudo chown -R $USER:www-data {app_dir}
```
```
echo "chmod-socket 664" >>{app}.ini
echo "uid: $USER" >>{app}.ini
echo "gid: www-data" >>{app}.ini
```

- **Issue:** [WARNING]: sftp transfer mechanism failed on [127.0.0.1]. Use ANSIBLE_DEBUG=1 to see detailed information
- **Resolution:** Force Ansible to use SCP mechanism instead of SFTP
```
cp /etc/ansible ./ansible/
sed 's|#transfer_method = smart|transfer_method = scp|g' ansible/ansible.cfg
```
```
Vagrantfile: ansible.config_file = "ansible/ansible.cfg"
```


THANK YOU:
---------

Thank you for taking the time to read this and for testing my work. Much appreciated


References:
-----------

Vagrant Setup:
https://www.vagrantup.com/docs/providers/basic_usage.html

Vagrant & Ansible Parallelism:
https://www.vagrantup.com/docs/provisioning/ansible.html

Vagrant-Ansible Shared Options (extra_vars, groups, etc):
https://www.vagrantup.com/docs/provisioning/ansible_common.html

Vagrant Guide (Ansible Context):
https://docs.ansible.com/ansible/latest/scenario_guides/guide_vagrant.html

Ansible Module Index:
https://docs.ansible.com/ansible/latest/modules/modules_by_category.html

Nginx/Ubuntu Installation:
https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-18-04

Nginx Load Balancing:
https://docs.nginx.com/nginx/admin-guide/load-balancer/http-load-balancer/

Nginx Log Variables:
http://nginx.org/en/docs/varindex.html

Flask Docs:
https://pypi.org/project/Flask/

Uwsgi & Flask Configuration:
https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uswgi-and-nginx-on-ubuntu-18-04

Uwsgi Configuration (Context: Permissions & Sockets):
https://uwsgi-docs.readthedocs.io/en/latest/tutorials/Django_and_nginx.html

Python 'requests' Library:
https://realpython.com/python-requests/

Siege Stress Testing:
https://linux.die.net/man/1/siege
  

