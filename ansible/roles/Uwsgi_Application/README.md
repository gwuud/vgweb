Uwsgi_Application:
=========

**Run a Flask application over Nginx (Proxying to uWSGI)**

Requirements
------------

N/A

Role Variables
--------------
**Role-level:**
```
Uwsgi_Service:
  domain: # endpoint domain name

  application:
    name: # the name of the application (this gets rendered into the service & nginx conf files)
    path: # path to the application you want to run
    owner: # the user that will run the uwsgi framework
```
**Play-level:**
```
domain: # endpoint domain name

application:
  name: # the name of the application (this gets rendered into the service & nginx conf files)
  path: # path to the application you want to run
  owner: # the user that will run the uwsgi framework
```
Dependencies
------------

N/A

Example Playbook
----------------
```
  tasks:
    - unarchive:
        src: mycode/
        dest: /home/somebody
        owner: somebody
    - include_role:
        name: Uwsgi_Application
```
License
-------

BSD

Author Information
------------------

Gareth Wood
