#!/bin/bash
  
HITS=20

for i in `seq 1 $HITS`
do
    curl "{{ endpoint }}" >/dev/null 2>&1
done

tail -$HITS /var/log/nginx/access.log