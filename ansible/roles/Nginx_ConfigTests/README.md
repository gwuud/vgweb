Nginx_ConfigTests
=========

**Various Testing of NGINX Server Configurations**
- **Load Balancer:**
  - Service state
  - Ping endpoint
  - Scrape endpoint
  - Upstream load balancing
  - Siege

<p>&nbsp;</p>

- **Application Server:**
  - Service state
  - Ping endpoint
  - Endpoint URI status

<p>&nbsp;</p>

  **NOTE:** The role works out what the servers function is based on the existence of certain
  files. For instance, a load balancer must have a */etc/nginx/conf.d/{}.conf*, and a web
  server should at least have an enabled server block. Therefore, these variables will need to be supplied

*Debug tasks, instead of being looped, are listed explicitly so that output is split up and, hopefully, easier to read*

Requirements
------------

N/A

Role Variables
--------------
**Role-level:**
```
Nginx_ConfigTests:
  webserver:
    block: # name of server block / domain name

  loadbalancer:
    config: # path to load balancer configuration file, 
            # ie, /etc/nginx/conf.d/load.conf
```
**Play-level:**
```
webserver:
  block: # name of server block / domain name

loadbalancer:
  config: # path to load balancer configuration file, 
          # ie, /etc/nginx/conf.d/load.conf
```
Dependencies
------------

N/A

Example Playbook
----------------
```
hosts: nginx_servers
vars:
  webserver:
    block: example.com
  loadbalancer: 
    config: /etc/nginx/conf.d/load.conf
  endpoint: example.com
roles:
  - Nginx_ConfigTests
```
License
-------

BSD

Author Information
------------------

Gareth Wood
