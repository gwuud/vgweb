Sudoers
=========

**Set Sudo Access to Users & Groups in /etc/sudoer.d/**

Requirements
------------

N/A

Role Variables
--------------
**Role-level:**
```
sudoers:
  - name: # name of unit to add to sudoers
    allpriv: # set sudoer with sudo:nopasswd (boolean)
```
**Play-level:**
```
- name: # name of unit to add to sudoers
  allpriv: # set sudoer with sudo:nopasswd (boolean)
```
Dependencies
------------

N/A

Example Playbook
----------------
```
hosts: all
vars:
  sudoer_name: somebody
  sudoer_allpriv: true
tasks:
  - user:
      name: somebody
      group: somegroup
  - include_role:
      name: Sudoers
```
License
-------

BSD

Author Information
------------------

Gareth Wood
