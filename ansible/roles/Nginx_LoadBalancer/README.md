Nginx_LoadBalancer
=========

**Configure A Resource As NGINX Load Balancer**

Requirements
------------

N/A

Role Variables
--------------
**Role-level:**
```
Nginx_LoadBalancer:
  config_file: # path to load balancer configuration file

  upstream:
    proxied: # list of hosts that nginx can proxy requests to
    method: # set load balancing method
```
**Play-level:**
```
config_file: # path to load balancer configuration file

upstream:
  proxied: # list of hosts that nginx can proxy requests to
  method: # set load balancing method
```
More info on nginx load balancing methods:
https://docs.nginx.com/nginx/admin-guide/load-balancer/http-load-balancer/

Dependencies
------------

N/A

Example Playbook
----------------
```
hosts: loadbalancers
vars:
  config_file: /etc/nginx/conf.d/load.conf
  upstream: 
    proxied: my_upstream_servers
    method: ip_hash
roles:
  - Nginx_LoadBalancer
```
License
-------

BSD

Author Information
------------------

Gareth Wood
